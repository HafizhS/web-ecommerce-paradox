<?php

namespace App\Repositories\Contracts;

interface BaseContractExtended
{
    public function allOrder($orderBy, $orderType);

    public function getOneWhere($column, $value, $with);

    public function getManyWhere($column, $value);

    public function countWhere($column, $value);

    public function datatable($select);

    public function datatableWith($select, array $with);
}