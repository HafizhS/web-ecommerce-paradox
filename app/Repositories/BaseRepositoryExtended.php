<?php
/**
 * Created by PhpStorm.
 * User: Hafizh
 * Date: 22/11/2019
 * Time: 16:14
 */

namespace App\Repositories;


use App\Repositories\Contracts\BaseContractExtended;

class BaseRepositoryExtended extends BaseRepository implements BaseContractExtended
{
    public function allOrder($orderBy, $orderType)
    {
    }

    public function getOneWhere($column, $value, $with)
    {
    }

    public function getManyWhere($column, $value)
    {
    }

    public function countWhere($column, $value)
    {

    }

    public function datatable($select)
    {
    }

    public function datatableWith($select, array $with)
    {
    }
}