<?php

namespace App\Repositories;

use App\Repositories\Contracts\BaseContract;
use Reliese\Coders\Model\Model;

class BaseRepository implements BaseContract
{
    protected $model;

    public function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public function get($id)
    {
        // TODO: Implement get() method.
    }

    public function create(array $data)
    {
        // TODO: Implement create() method.
    }

    public function update($id, array $data)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}