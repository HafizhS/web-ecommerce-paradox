<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Dec 2019 06:25:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderStatus
 *
 * @property int $id_order_status
 * @property string $status
 * @property string $desc
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @package App\Models
 * @property-read int|null $orders_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus whereIdOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderStatus whereStatus($value)
 * @mixin \Eloquent
 */
class OrderStatus extends Eloquent
{
	protected $table = 'order_status';
	protected $primaryKey = 'id_order_status';
	public $timestamps = false;

	protected $fillable = [
		'status',
		'desc'
	];

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class, 'id_order_status');
	}
}
