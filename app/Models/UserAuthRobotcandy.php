<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 23 Dec 2019 03:06:45 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAuthRobotcandy
 *
 * @property int $robotcandy_id_owner
 * @property string $robotcandy_auth_token
 * @property \Carbon\Carbon $robotcandy_time_generated
 * @property \Carbon\Carbon $robotcandy_time_live
 * @property \App\Models\User $user
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy whereRobotcandyAuthToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy whereRobotcandyIdOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy whereRobotcandyTimeGenerated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAuthRobotcandy whereRobotcandyTimeLive($value)
 * @mixin \Eloquent
 */
class UserAuthRobotcandy extends Eloquent
{
	protected $table = 'user_auth_robotcandy';
	protected $primaryKey = 'robotcandy_id_owner';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'robotcandy_id_owner' => 'int'
	];

	protected $dates = [
		'robotcandy_time_generated',
		'robotcandy_time_live'
	];

	protected $hidden = [
		'robotcandy_auth_token'
	];

	protected $fillable = [
		'robotcandy_auth_token',
		'robotcandy_time_generated',
		'robotcandy_time_live'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'robotcandy_id_owner');
	}
}
