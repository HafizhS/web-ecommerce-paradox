<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Kategori
 *
 * @property int $id_kategori
 * @property string $nama_kategori
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 * @package App\Models
 * @property-read int|null $barangs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kategori newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kategori newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kategori query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kategori whereIdKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kategori whereNamaKategori($value)
 * @mixin \Eloquent
 */
class Kategori extends Eloquent
{
	protected $table = 'kategori';
	protected $primaryKey = 'id_kategori';
	public $timestamps = false;

	protected $fillable = [
		'nama_kategori'
	];

	public function barangs()
	{
		return $this->belongsToMany(\App\Models\Barang::class, 'barang_kategori', 'id_kategori', 'id_barang')
					->withPivot('id_barang_kategori');
	}
}
