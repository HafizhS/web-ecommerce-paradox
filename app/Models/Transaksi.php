<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Dec 2019 06:25:25 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Transaksi
 *
 * @property int $id_transaksi
 * @property int $id_order
 * @property int $id_detail_transaksi
 * @property \App\Models\DetailTransaksi $detail_transaksi
 * @property \App\Models\Order $order
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi whereIdDetailTransaksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaksi whereIdTransaksi($value)
 * @mixin \Eloquent
 */
class Transaksi extends Eloquent
{
	protected $table = 'transaksi';
	protected $primaryKey = 'id_transaksi';
	public $timestamps = false;

	protected $casts = [
		'id_order' => 'int',
		'id_detail_transaksi' => 'int'
	];

	protected $fillable = [
		'id_order',
		'id_detail_transaksi'
	];

	public function detail_transaksi()
	{
		return $this->belongsTo(\App\Models\DetailTransaksi::class, 'id_detail_transaksi');
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class, 'id_order');
	}
}
