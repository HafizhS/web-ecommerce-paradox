<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Reliese\Database\Eloquent\Model;

/**
 * Class Barang
 *
 * @property int $id_barang
 * @property string $nama_barang
 * @property string $deskripsi
 * @property int $harga
 * @property boolean $gambar_barang
 * @property \Illuminate\Database\Eloquent\Collection $kategoris
 * @property \Illuminate\Database\Eloquent\Collection $barang_transaksis
 * @property \Illuminate\Database\Eloquent\Collection $gudangs
 * @property \Illuminate\Database\Eloquent\Collection $keranjangs
 * @package App\Models
 * @property-read int|null $barang_transaksis_count
 * @property-read int|null $gudangs_count
 * @property-read int|null $kategoris_count
 * @property-read int|null $keranjangs_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereDeskripsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereGambarBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereHarga($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereNamaBarang($value)
 * @mixin \Eloquent
 */
class Barang extends Eloquent
{
    protected $table = 'barang';
    protected $primaryKey = 'id_barang';
    public $timestamps = false;

    protected $casts = [
        'harga' => 'int',
//        'gambar_barang' => 'boolean'
    ];

    protected $fillable = [
        'nama_barang',
        'deskripsi',
        'harga',
        'berat',
        'gambar_barang'
    ];

    public function kategoris()
    {
        return $this->belongsToMany(\App\Models\Kategori::class, 'barang_kategori', 'id_barang', 'id_kategori')
            ->withPivot('id_barang_kategori');
    }

    public function kategoriByName($kategori)
    {
        return $this->belongsToMany(\App\Models\Kategori::class, 'barang_kategori', 'id_barang', 'id_kategori')->where('nama_kategori', $kategori)
            ->withPivot('id_barang_kategori');
    }

    public function barang_transaksis()
    {
        return $this->hasMany(\App\Models\BarangTransaksi::class, 'id_barang');
    }

    public function gudangs()
    {
        return $this->hasMany(\App\Models\Gudang::class, 'id_barang');
    }

    public function keranjangs()
    {
        return $this->hasMany(\App\Models\Keranjang::class, 'id_barang');
    }
}
