<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Keranjang
 *
 * @property int $id_keranjang
 * @property int $id_user
 * @property int $id_barang
 * @property \App\Models\Barang $barang
 * @property \App\Models\User $user
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang whereIdKeranjang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang whereIdUser($value)
 * @mixin \Eloquent
 * @property int $quantity
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Keranjang whereQuantity($value)
 */
class Keranjang extends Eloquent
{
	protected $table = 'keranjang';
	protected $primaryKey = 'id_keranjang';
	public $timestamps = false;

	protected $casts = [
		'id_user' => 'int',
		'id_barang' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_barang',
        'quantity'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_user');
	}
}
