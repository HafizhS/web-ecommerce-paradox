<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Dec 2019 06:24:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 *
 * @property int $id_order
 * @property int $id_order_status
 * @property int $id_user
 * @property string $courier_order
 * @property string $alamat_tujuan
 * @property string $alamat_pengirim
 * @property \Carbon\Carbon $tanggal_order
 * @property \Carbon\Carbon $tanggal_dikirim
 * @property \Carbon\Carbon $tanggal_sampai
 * @property \App\Models\OrderStatus $order_status
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $barangs
 * @property \Illuminate\Database\Eloquent\Collection $transaksis
 * @package App\Models
 * @property-read int|null $barangs_count
 * @property-read int|null $transaksis_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAlamatPengirim($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAlamatTujuan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCourierOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereIdOrderStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTanggalDikirim($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTanggalOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereTanggalSampai($value)
 * @mixin \Eloquent
 */
class Order extends Eloquent
{
	protected $table = 'order';
	protected $primaryKey = 'id_order';
	public $timestamps = false;

	protected $casts = [
		'id_order_status' => 'int',
		'id_user' => 'int'
	];

	protected $dates = [
		'tanggal_order',
		'tanggal_dikirim',
		'tanggal_sampai'
	];

	protected $fillable = [
		'id_order_status',
		'id_user',
		'courier_order',
		'alamat_tujuan',
		'alamat_pengirim',
		'tanggal_order',
		'tanggal_dikirim',
		'tanggal_sampai'
	];

	public function order_status()
	{
		return $this->belongsTo(\App\Models\OrderStatus::class, 'id_order_status');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_user');
	}

	public function barangs()
	{
		return $this->belongsToMany(\App\Models\Barang::class, 'order_barang', 'id_order', 'id_barang')
					->withPivot('id_barang_transaksi', 'quantity');
	}

	public function transaksis()
	{
		return $this->hasMany(\App\Models\Transaksi::class, 'id_order');
	}
}
