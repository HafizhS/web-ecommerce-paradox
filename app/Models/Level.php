<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Level
 *
 * @property int $id_level
 * @property string $nama_level
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @package App\Models
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Level newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Level newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Level query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Level whereIdLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Level whereNamaLevel($value)
 * @mixin \Eloquent
 */
class Level extends Eloquent
{
	protected $table = 'level';
	protected $primaryKey = 'id_level';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'id_level' => 'int'
	];

	protected $fillable = [
		'nama_level'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'id_level');
	}
}
