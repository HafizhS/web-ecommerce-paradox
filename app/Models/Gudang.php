<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Gudang
 *
 * @property int $id_gudang
 * @property int $id_barang
 * @property int $stok_barang
 * @property \App\Models\Barang $barang
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang whereIdGudang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gudang whereStokBarang($value)
 * @mixin \Eloquent
 */
class Gudang extends Eloquent
{
	protected $table = 'gudang';
	protected $primaryKey = 'id_gudang';
	public $timestamps = false;

	protected $casts = [
		'id_barang' => 'int',
		'stok_barang' => 'int'
	];

	protected $fillable = [
		'id_barang',
		'stok_barang'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}
}
