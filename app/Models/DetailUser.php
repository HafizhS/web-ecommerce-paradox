<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 13 Dec 2019 07:12:31 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DetailUser
 *
 * @property int $id_detail_user
 * @property string $username
 * @property string $nama_lengkap
 * @property string $jenis_kelamin
 * @property boolean $avatar
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @package App\Models
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser whereIdDetailUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser whereJenisKelamin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser whereNamaLengkap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailUser whereUsername($value)
 * @mixin \Eloquent
 */
class DetailUser extends Eloquent
{
	protected $table = 'detail_user';
	protected $primaryKey = 'id_detail_user';
	public $timestamps = false;

	protected $casts = [
		'avatar' => 'boolean'
	];

	protected $fillable = [
		'username',
		'nama_lengkap',
		'jenis_kelamin',
		'avatar'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'id_detail_user');
	}
}
