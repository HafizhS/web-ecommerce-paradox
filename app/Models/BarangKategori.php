<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class BarangKategori
 *
 * @property int $id_barang_kategori
 * @property int $id_barang
 * @property int $id_kategori
 * @property \App\Models\Barang $barang
 * @property \App\Models\Kategori $kategori
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori whereIdBarangKategori($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BarangKategori whereIdKategori($value)
 * @mixin \Eloquent
 */
class BarangKategori extends Eloquent
{
	protected $table = 'barang_kategori';
	protected $primaryKey = 'id_barang_kategori';
	public $timestamps = false;

	protected $casts = [
		'id_barang' => 'int',
		'id_kategori' => 'int'
	];

	protected $fillable = [
		'id_barang',
		'id_kategori'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function kategori()
	{
		return $this->belongsTo(\App\Models\Kategori::class, 'id_kategori');
	}
}
