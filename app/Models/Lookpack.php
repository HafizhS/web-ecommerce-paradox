<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 10 Oct 2019 03:01:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Lookpack
 *
 * @property int $id_lookpack
 * @property string $nama_pack
 * @property string $desc
 * @property mediumblob $model_pack
 * @property string $jenis_kelamin
 * @property \Illuminate\Database\Eloquent\Collection $lookpack_items
 * @package App\Models
 * @property-read int|null $lookpack_items_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack whereIdLookpack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack whereJenisKelamin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack whereModelPack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Lookpack whereNamaPack($value)
 * @mixin \Eloquent
 */
class Lookpack extends Eloquent
{
	protected $table = 'lookpack';
	protected $primaryKey = 'id_lookpack';
	public $timestamps = false;

	protected $casts = [
		'model_pack' => 'mediumblob'
	];

	protected $fillable = [
		'nama_pack',
		'desc',
		'model_pack',
		'jenis_kelamin'
	];

	public function lookpack_items()
	{
		return $this->hasMany(\App\Models\LookpackItem::class, 'id_lookpack');
	}
}
