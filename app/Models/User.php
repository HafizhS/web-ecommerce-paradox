<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 07:21:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 *
 * @property int $id_user
 * @property string $email
 * @property string $password
 * @property int $id_detail_user
 * @property int $id_level
 * @property \App\Models\DetailUser $detail_user
 * @property \App\Models\Level $level
 * @property \Illuminate\Database\Eloquent\Collection $keranjangs
 * @property \Illuminate\Database\Eloquent\Collection $transaksis
 * @package App\Models
 * @property-read int|null $keranjangs_count
 * @property-read int|null $transaksis_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIdDetailUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIdLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @mixin \Eloquent
 */
class User extends Eloquent
{
	protected $table = 'user';
	protected $primaryKey = 'id_user';
	public $timestamps = false;

	protected $casts = [
		'id_detail_user' => 'int',
		'id_level' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'email',
		'password',
		'id_detail_user',
		'id_level'
	];

	public function detail_user()
	{
		return $this->belongsTo(\App\Models\DetailUser::class, 'id_detail_user');
	}

	public function level()
	{
		return $this->belongsTo(\App\Models\Level::class, 'id_level');
	}

	public function keranjangs()
	{
		return $this->hasMany(\App\Models\Keranjang::class, 'id_user');
	}

	public function transaksis()
	{
		return $this->hasMany(\App\Models\Transaksi::class, 'id_user');
	}
}
