<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Dec 2019 13:48:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class RoProvince
 * 
 * @property int $id_province
 * @property string $province
 * 
 * @property \Illuminate\Database\Eloquent\Collection $ro_cities
 * @property \App\Models\UserAlamatRajaongkir $user_alamat_rajaongkir
 *
 * @package App\Models
 */
class RoProvince extends Eloquent
{
	protected $table = 'ro_province';
	protected $primaryKey = 'id_province';
	public $timestamps = false;

	protected $fillable = [
		'province'
	];

	public function ro_cities()
	{
		return $this->hasMany(\App\Models\RoCity::class, 'id_province');
	}

	public function user_alamat_rajaongkir()
	{
		return $this->hasOne(\App\Models\UserAlamatRajaongkir::class, 'ro_province');
	}
}
