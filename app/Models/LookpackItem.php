<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 08 Oct 2019 03:52:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class LookpackItem
 *
 * @property int $id_lookpack_item
 * @property int $id_barang
 * @property int $id_lookpack
 * @property \App\Models\Barang $barang
 * @property \App\Models\Lookpack $lookpack
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem whereIdLookpack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\LookpackItem whereIdLookpackItem($value)
 * @mixin \Eloquent
 */
class LookpackItem extends Eloquent
{
	protected $table = 'lookpack_item';
	protected $primaryKey = 'id_lookpack_item';
	public $timestamps = false;

	protected $casts = [
		'id_barang' => 'int',
		'id_lookpack' => 'int'
	];

	protected $fillable = [
		'id_barang',
		'id_lookpack'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function lookpack()
	{
		return $this->belongsTo(\App\Models\Lookpack::class, 'id_lookpack');
	}
}
