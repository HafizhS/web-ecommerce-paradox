<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Dec 2019 06:25:14 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class OrderBarang
 *
 * @property int $id_barang_transaksi
 * @property int $id_order
 * @property int $id_barang
 * @property int $quantity
 * @property \App\Models\Barang $barang
 * @property \App\Models\Order $order
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang whereIdBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang whereIdBarangTransaksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang whereIdOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OrderBarang whereQuantity($value)
 * @mixin \Eloquent
 */
class OrderBarang extends Eloquent
{
	protected $table = 'order_barang';
	protected $primaryKey = 'id_barang_transaksi';
	public $timestamps = false;

	protected $casts = [
		'id_order' => 'int',
		'id_barang' => 'int',
		'quantity' => 'int'
	];

	protected $fillable = [
		'id_order',
		'id_barang',
		'quantity'
	];

	public function barang()
	{
		return $this->belongsTo(\App\Models\Barang::class, 'id_barang');
	}

	public function order()
	{
		return $this->belongsTo(\App\Models\Order::class, 'id_order');
	}
}
