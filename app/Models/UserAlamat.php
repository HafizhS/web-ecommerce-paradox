<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 16 Dec 2019 06:53:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserAlamat
 *
 * @property int $id_user_alamat
 * @property int $id_user
 * @property string $nama_penerima
 * @property string $alamat_lengkap
 * @property int $ro_provinsi
 * @property int $ro_kota
 * @property int $kodepos
 * @property \App\Models\User $user
 * @package App\Models
 * @property string|null $nama_pemilik
 * @property string|null $alamat_kota
 * @property string|null $alamat_provinsi
 * @property int|null $alamat_kodepos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereAlamatKodepos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereAlamatKota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereAlamatLengkap($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereAlamatProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereIdUser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereIdUserAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserAlamat whereNamaPemilik($value)
 * @mixin \Eloquent
 */
class UserAlamat extends Eloquent
{
	protected $table = 'user_alamat';
	protected $primaryKey = 'id_user_alamat';
	public $timestamps = false;

	protected $casts = [
		'id_user' => 'int',
		'ro_provinsi' => 'int',
		'ro_kota' => 'int',
		'kodepos' => 'int'
	];

	protected $fillable = [
		'id_user',
		'nama_penerima',
		'alamat_lengkap',
		'ro_provinsi',
		'ro_kota',
		'kodepos'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'id_user');
	}
}
