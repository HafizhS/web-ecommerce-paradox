<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 04 Dec 2019 06:25:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class DetailTransaksi
 *
 * @property int $id_detail_transaksi
 * @property \Carbon\Carbon $tanggal_transaksi
 * @property \Carbon\Carbon $tanggal_dibayar
 * @property string $status_transaksi
 * @property float $total_bayar
 * @property \Illuminate\Database\Eloquent\Collection $transaksis
 * @package App\Models
 * @property-read int|null $transaksis_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi whereIdDetailTransaksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi whereStatusTransaksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi whereTanggalDibayar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi whereTanggalTransaksi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\DetailTransaksi whereTotalBayar($value)
 * @mixin \Eloquent
 */
class DetailTransaksi extends Eloquent
{
	protected $table = 'detail_transaksi';
	protected $primaryKey = 'id_detail_transaksi';
	public $timestamps = false;

	protected $casts = [
		'total_bayar' => 'float'
	];

	protected $dates = [
		'tanggal_transaksi',
		'tanggal_dibayar'
	];

	protected $fillable = [
		'tanggal_transaksi',
		'tanggal_dibayar',
		'status_transaksi',
		'total_bayar'
	];

	public function transaksis()
	{
		return $this->hasMany(\App\Models\Transaksi::class, 'id_detail_transaksi');
	}
}
