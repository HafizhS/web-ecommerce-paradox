<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 29 Dec 2019 13:49:01 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class RoCity
 * 
 * @property int $id_city
 * @property int $id_province
 * @property string $province
 * @property string $type
 * @property string $city_name
 * @property int $postal_code
 * 
 * @property \App\Models\RoProvince $ro_province
 * @property \App\Models\UserAlamatRajaongkir $user_alamat_rajaongkir
 *
 * @package App\Models
 */
class RoCity extends Eloquent
{
	protected $table = 'ro_city';
	protected $primaryKey = 'id_city';
	public $timestamps = false;

	protected $casts = [
		'id_province' => 'int',
		'postal_code' => 'int'
	];

	protected $fillable = [
		'id_province',
		'province',
		'type',
		'city_name',
		'postal_code'
	];

	public function ro_province()
	{
		return $this->belongsTo(\App\Models\RoProvince::class, 'id_province');
	}

	public function user_alamat_rajaongkir()
	{
		return $this->hasOne(\App\Models\UserAlamatRajaongkir::class, 'ro_city');
	}
}
