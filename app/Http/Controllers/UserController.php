<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\DetailUser;
use App\Models\Level;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with("level")->get();
//        foreach ($users as $user) {
//            echo $user->level;
//        }
//        return response()->json($users,200,array(),JSON_PRETTY_PRINT);
        return view("admin.datauser")->with("users", $users);
    }

    public function indexApi()
    {
        $users = User::all();
        return response()->json($users, 200, array(), JSON_PRETTY_PRINT);
    }

    public function loginApi(Request $request)
    {
        $user = User::where("email", $request->input("email"))->where("password", $request->input("password"))->first();
        if ($user == null) {
            $respone["message"] = "Login Failed";
            $respone["status"] = 0;
        } else {
            $respone["message"] = "Login Success";
            $respone["id_user"] = $user->id_user;
            $respone["email"] = $user->email;
            $respone["id_detail_user"] = $user->id_detail_user;
            $respone["level"] = $user->id_detail_user;
            $respone["level"] = $user->id_level;
            $respone["status"] = 1;
            $respone["rajaongkir_token"] = "2f9422428e3c7bad1b089ce9f552ba04";
        }
        return response()->json($respone);
    }

    public function register(Request $request)
    {
        error_log($request->ip() . ' send request TO ' . $request->method() . ':' . $request->route()->getActionName() . " WITH " . $request->headers->get('user-agent'));
        $result = [
            'message' => '',
            'status' => '0',
            'detail' => [
                'isDataValid' => false,
                'isSuccess' => [
                    'user' => null,
                    'detail_user' => null
                ]
            ]
        ];

        $rule = [
            'email' => 'required',
            'username' => 'required',
            'nama_lengkap' => 'required',
            'password' => 'required'
        ];
//
        try {
            $this->validate($request, $rule);
        } catch (ValidationException $e) {
            $result['detail']['isDataValid'] = false;
            $result['message'] = 'Data is not valid';
            return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
        }

        $result['detail']['isDataValid'] = true;

        $detail = new DetailUser();
        $detail->username = $request->input('username');
        $detail->nama_lengkap = $request->input('nama_lengkap');
        $result['detail']['isSuccess']['detail_user'] = $detail->save();

        $user = new User();
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->id_detail_user = $detail->id_detail_user;

        $result['detail']['isSuccess']['user'] = $user->save();

        if ($result['detail']['isSuccess']['user'] && $result['detail']['isSuccess']['detail_user']) {
            $result['message'] = 'Registration Success';
            $result['status'] = 1;
        } else {
            $result['message'] = 'Registration Failed, Please try again';
        }

        return response()->json($result, 200, array(), JSON_PRETTY_PRINT);

    }

    public function getDetailUser(Request $request)
    {
        $user = \DB::table('detail_user')
            ->join('user', 'user.id_detail_user', '=', 'detail_user.id_detail_user')
            ->where('user.id_detail_user', '=', $request->input('id_detail_user'))
            ->select('*')
            ->first();

//        return response()->json($user, 200, array(), JSON_PRETTY_PRINT);
        return view('admin.detailuser')->with('user', $user);

    }

    public function getDetailUserApi(Request $request)
    {
//        try {
//            $rule = [
//                'id_user' => 'required|int'
//            ];
//
//            $this->validate($request, $rule);
//        } catch (ValidationException $e) {
//            return response()->json($e->getMessage());
//        }

        $user = \DB::table('detail_user')
            ->join('user', 'user.id_detail_user', '=', 'detail_user.id_detail_user')
            ->where('user.id_detail_user', '=', $request->input('id_detail_user'))
            ->select('*')
            ->first();

        return response()->json($user, 200, array(), JSON_PRETTY_PRINT);
    }

    public function test321()
    {
        $array1['nama'] = "hafizh";
        $array1['detail']['message'] = "test321";
        $array1['detail']['status'] = 0;


        $array2 = [
            'nama' => "hafizh",
            'status' => 0
        ];

        return response()->json($array1['detail']['status'], 200, array(), JSON_PRETTY_PRINT);
    }
}
