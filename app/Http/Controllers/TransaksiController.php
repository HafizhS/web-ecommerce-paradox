<?php

namespace App\Http\Controllers;

use App\Models\BarangTransaksi;
use App\Models\DetailTransaksi;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use phpDocumentor\Reflection\Types\Integer;

class TransaksiController extends Controller
{
    public function createTransaction(Request $request)
    {

//        dd($request);

        $now = Carbon::now()->toDateTimeString();

        $rule = [
            'method' => 'required',
            'total_bayar' => 'required',
            'id_user' => 'required',
            'barang' => 'required|array',
            'barang.*' => 'required|array|max:2',
            'barang.*.*' => 'required|int'
        ];


        $result = [
            "message" => "Unknown Error!",
            "status" => 0,
            "isSuccess" => false
        ];

        try {
            $this->validate($request, $rule);
        } catch (ValidationException $exception) {
            $result = [
                "message" => $exception->getMessage(),
                "status" => 0,
                "isSuccess" => false
            ];
            return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
        }

        $detailTransaksi = new DetailTransaksi();
        $detailTransaksi->tanggal_transaksi = $now;
        $detailTransaksi->total_bayar = $request->input('total_bayar');
        if ($request->input('method') == "paynow") {
            $detailTransaksi->tanggal_dibayar = $now;
            $detailTransaksi->status_transaksi = "dibayar";
        } else {
            $detailTransaksi->status_transaksi = "pending";
        }
        $status['detail'] = $detailTransaksi->save();

        $transaksi = new Transaksi();
        $transaksi->id_user = $request->input('id_user');
        $transaksi->id_detail_transaksi = $detailTransaksi->id_detail_transaksi;
        $status['transaksi'] = $transaksi->save();

        foreach ($request->input('barang') as $value) {
            if (is_array($value) || !empty($value)) {
                $barangTransaksi = new BarangTransaksi();
                $barangTransaksi->id_barang = $value[0];
                $barangTransaksi->id_transaksi = $transaksi->id_transaksi;
                $barangTransaksi->quatity = $value[1];
                $status['barang'] = $barangTransaksi->save();
            } else {
                $result['message'] = "Invalid data parameter";
                $result['status'] = 0;
                $result['isSuccess'] = false;
                return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
            }
        }

        if ($status['detail'] && $status['transaksi'] && $status['barang']) {
            $result['message'] = "Success make transaction!";
            $result['status'] = 1;
            $result['isSuccess'] = true;
        } else {
            $result['message'] = "Failed make transaction!";
            $result['status'] = 0;
            $result['isSuccess'] = false;
        }

        return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
    }
}
