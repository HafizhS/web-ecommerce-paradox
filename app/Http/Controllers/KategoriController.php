<?php

namespace App\Http\Controllers;

use App\Mail\test1;
use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{

    function indexView()
    {
        $data = Kategori::all();
        return view('admin.datakategori')->with('data', $data);
    }

    function getAll()
    {
        return response()->json(Kategori::orderBy('nama_kategori')->get(), 200, array(), JSON_PRETTY_PRINT);
    }

    function getAllArray()
    {
        $data = Kategori::orderBy("nama_kategori")->get();
        return view("test")->with("kategoris", $data);
    }

    function store(Request $request)
    {
        $kategori = new Kategori();
        $kategori->nama_kategori = $request->input('name');
        $result['kategori'] = $kategori->save();
        if ($result['kategori']) {
            $result['message'] = "Success adding new category! (new id: " . $kategori->id_kategori . ", name: " . $kategori->nama_kategori . ")";
            $result['theme'] = "callout-success";
        } else {
            $result['message'] = "Failes adding new category!";
            $result['theme'] = "callout-danger";
        }
        return redirect(route('admin.datakategori'))->with('result', $result);

    }

    function delete(Request $request)
    {
        $result['update'] = Kategori::where('id_kategori', $request->input('id'))->delete();
        if ($result['update'] == 1) {
            $result['message'] = "Success deleting category (id:" . $request->input('id') . ", name: " . $request->input('currentname') . ")";
            $result['theme'] = "callout-success";
        } else if ($result['update'] == 0) {
            $result['message'] = "Failed deleting category!! (id:" . $request->input('id') . ", name: " . $request->input('name') . ")";
            $result['theme'] = "callout-danger";
        } else {
            $result['message'] = "Unknown status result!! (id:" . $request->input('id') . ")";
            $result['theme'] = "callout-warning";
        }
        return redirect(route('admin.datakategori'))->with('result', $result);
    }

    function edit(Request $request)
    {
        $result['update'] = Kategori::where('id_kategori', $request->input('id'))->update([
            "nama_kategori" => $request->input('name')
        ]);
        if ($result['update'] == 1) {
            $result['message'] = "Success editing category (id:" . $request->input('id') . ", with new name: " . $request->input('name') . ")";
            $result['theme'] = "callout-success";
        } else if ($result['update'] == 0) {
            $result['message'] = "Failed editing category! (id:" . $request->input('id') . ")";
            $result['theme'] = "callout-danger";
        } else {
            $result['message'] = "Unknown status result!! (id:" . $request->input('id') . ")";
            $result['theme'] = "callout-warning";
        }
        return redirect(route('admin.d5atakategori'))->with('result', $result);
    }

    function testdd(Request $request)
    {
        dd($request);
    }
}
