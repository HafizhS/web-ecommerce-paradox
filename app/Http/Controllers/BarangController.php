<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\Kategori;
use App\Test;
use App\User;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Mockery\Matcher\Closure;
use Reliese\Meta\MySql\Schema;

class BarangController extends Controller
{
    public function create()
    {
        $data = Kategori::all();
        return view("admin.tambahbarang")->with("data", $data);
    }

    public function store(Request $request)
    {
//        dd($request);
        $barang = new Barang();
        $barang->nama_barang = $request->input("nama_barang");
        $barang->deskripsi = $request->input("deskripsi");
        $barang->harga = $request->input("harga");
        if ($request->hasFile('image')) {
            $barang->gambar_barang = $request->file("image")->get();
        }
        $result['barang'] = $barang->save();

        if (!empty($request->input("category"))) {
            foreach ($request->input("category") as $cat) {
                $category = new BarangKategori();
                $category->id_kategori = Kategori::all()->where("nama_kategori", $cat)->first()->id_kategori;
                $category->id_barang = $barang->id_barang;
                $result['barangkategori'] = $category->save();
            }
        }

        return route('admin.databarang');
//        return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
    }

    public function update(Request $request)
    {
        $barang = Barang::where("id_barang", $request->input("id_barang"));
        $barang->update($request->except('_token', '_method'));

        $respone["barang"] = $barang;
        $respone["success"] = 1;

        return response()->json($respone);
    }

    public function destroy($id)
    {
        $barang = DB::table("barang")->where("id_barang", $id)->delete();
        return response()->json("Remove successfully.");
    }

    public function index()
    {
        $barangs = Barang::all();
        foreach ($barangs as $barang) {
            $image = base64_encode($barang->gambar_barang);
            $image = str_replace('data:image/jpg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $barang->gambar_barang = $image;
        }
        return response()->json($barangs, 200, array(), JSON_PRETTY_PRINT);
    }

    public function indexView()
    {
        $data = Barang::with("kategoris")->get();
        foreach ($data as $item) {
            $item->gambar_barang = base64_encode($item->gambar_barang);
//            foreach ($item['kategoris'] as $kategori) {
//                echo $kategori['nama_kategori']." ";
//            }
//            echo "<br>";
        }
        return view("admin.databarang")->with("data",$data);
//        return response()->json($data,200,array(),JSON_PRETTY_PRINT);
    }

    public function getImage($data)
    {
        $request = base64_encode($data);
        $image = $request;  // your base64 encoded
        $image = str_replace('data:image/jpg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'jpg';
        \File::put(storage_path() . '/' . $imageName, base64_decode($image));
        printf($request);
    }

    public function indexWithCategory(Request $request)
    {
//        dd($request);
        if (empty($request->input("category"))) {
            $barang = Barang::all();
            $respone = $barang;
            foreach ($respone as $item) {
                $item->kategoris;
            }
        } else {
            $barang = Barang::whereHas('kategoris', function ($query) use (&$request) {
                $query->where('nama_kategori', $request->input("category.0"));
            })->whereHas('kategoris', function ($q) use (&$request) {
                $q->where('nama_kategori', 'like', '%' . $request->input("category.1") . '%');
            })->get();
            $respone = $barang;
            foreach ($respone as $item) {
                $item->kategoris;
            }
        }

        foreach ($barang as $item) {
            $item->gambar_barang = base64_encode($item->gambar_barang);
//            $item->gambar_barang = 'ada';
        }

        return response()->json($respone, 200, array(), JSON_PRETTY_PRINT);
    }

    public static function convert_to_rupiah($angka)
    {
        return 'Rp. ' . strrev(implode('.', str_split(strrev(strval($angka)), 3)));
    }
}

