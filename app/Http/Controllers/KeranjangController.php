<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Keranjang;
use Doctrine\DBAL\Schema\Table;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class KeranjangController extends Controller
{
    public function addToKeranjang(Request $request)
    {
        $rule = [
            'id_barang' => 'required',
            'id_user' => 'required',
            'quantity' => 'required',
        ];
        try {
            $this->validate($request, $rule);
        } catch (ValidationException $e) {
            $result['message'] = "Data is not valid!";
            $result['status'] = 0;
            $result['isSuccess'] = false;
            return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
        }

        $check = Keranjang::all()->where("id_barang", "=", $request->input('id_barang'));

        if ($check->isNotEmpty()) {
            $result['message'] = "Item already on your cart";
            $result['status'] = 0;
            $result['isSuccess'] = false;
            return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
        }

        $keranjang = new Keranjang();
        $keranjang->id_user = $request->input('id_user');
        $keranjang->id_barang = $request->input('id_barang');
        $keranjang->quantity = $request->input('quantity');
        $result['isSuccess'] = $keranjang->save();
        if ($result['isSuccess']) {
            $result['message'] = "Item added to your cart";
            $result['status'] = 1;
        } else {
            $result['message'] = "Cant add item to your cart";
            $result['status'] = 0;
        }
        return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getUserCart(Request $request)
    {
//        $barang = \DB::table('barang')
//            ->join('keranjang', 'keranjang.id_barang', '=', 'barang.id_barang')
//            ->join('user', 'keranjang.id_user', '=', 'user.id_user')
//            ->where('user.id_user', '=', $request->input('id_user'))
//            ->select('barang.*')
//            ->get();

        $barang = Barang::whereHas('keranjangs', function ($query) use (&$request) {
            $query->where('id_user', $request->input("id_user"));
        })->with([
            'keranjangs' => function ($query) use (&$request) {
                $query->where('id_user', $request->input("id_user"));
            }
        ])->get();

        foreach ($barang as $item) {
//            $item->gambar_barang = base64_encode($item->gambar_barang);
            $item->gambar_barang = '';
            $total = 0;
            foreach ($item['keranjangs'] as $keranjang) {
                $total += 1;
            }
            $item['quantity'] = $total;
        }
        return response()->json($barang, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getAllCartItem(Request $request)
    {
//        $barang = Barang::whereHas('keranjangs', function ($query) use (&$request) {
//            $query->where('id_user', $request->input("id_user"));
//        })->with([
//            'keranjangs' => function($query) use (&$request) {
//                $query->where('id_user', $request->input("id_user"));
//            }
//        ])->get();

//        $barang = Barang::whereHas('keranjangs', function ($query) use (&$request) {
//            $query->where('id_user', $request->input("id_user"));
//        })->with([
//            'keranjangs' => function ($query) use (&$request) {
//                $query->where('id_user', $request->input("id_user"));
//            }
//        ])->get();

        $barang = \DB::table('barang')
            ->join('keranjang', 'keranjang.id_barang', '=', 'barang.id_barang')
            ->join('user', 'keranjang.id_user', '=', 'user.id_user')
            ->where('user.id_user', '=', $request->input('id_user'))
            ->select(['barang.id_barang','keranjang.id_keranjang','barang.nama_barang','barang.harga','barang.gambar_barang','keranjang.quantity'])
            ->get();

        foreach ($barang as $item) {
            $item->gambar_barang = base64_encode($item->gambar_barang);
//            $item->gambar_barang = '';
        }
        
        return response()->json($barang, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getAll(Request $request)
    {
        $keranjang = Keranjang::all();
        return response()->json($keranjang, 200, array(), JSON_PRETTY_PRINT);

    }

    public function test123()
    {
        $comments = [
            [
                'id_comment' => 0,
                'user' => 'a',
                'comment' => "saya goblok"
            ],
            [
                'id_comment' => 1,
                'user' => 'a',
                'comment' => "Semua anjing"
            ],
            [
                'id_comment' => 2,
                'user' => 'aabc',
                'comment' => "fuck this life"
            ]
        ];
        $words = [
            [
                'kasar' => 'goblok',
                'censor' => '******'
            ],
            [
                'kasar' => 'anjing',
                'censor' => '******'
            ],
            [
                'kasar' => 'fuck',
                'censor' => '****'
            ],

        ];

        for ($i = 0; $i <= sizeof($comments) - 1; $i++) {
            foreach ($words as $word) {
                $comments[$i]['comment'] = str_replace($word['kasar'], $word['censor'], $comments[$i]['comment']);
            }
        }

        return response()->json($comments, 200, array(), JSON_PRETTY_PRINT);
    }
}
