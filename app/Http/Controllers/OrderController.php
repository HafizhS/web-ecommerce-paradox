<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\DetailTransaksi;
use App\Models\Order;
use App\Models\OrderBarang;
use App\Models\OrderStatus;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    public function makeOrder(Request $request)
    {
//        dd($request->all());
        $rule = [
            'total_bayar' => 'required',
            'id_user' => 'required|int',
            'courier' => 'required',
            'alamat' => 'required',
            'barang' => 'required|array',
            'barang.*' => 'required|array|max:2',
            'barang.*.*' => 'required|int'
        ];

        $result = [
            "message" => "Unknown Error!",
            "status" => 0,
            "isSuccess" => false
        ];

        try {
            $this->validate($request, $rule);
        } catch (ValidationException $exception) {
            $result['message'] = $exception->getMessage();
            return response()->json($result);
        }

        $order = new Order();
        $order->id_order_status = OrderStatus::all()->where("status", "=", "pending")->first()->id_order_status;
        $order->id_user = $request->input('id_user');
        $order->courier_order = $request->input('courier');
        $order->alamat_tujuan = $request->input('alamat');
        $order->tanggal_order = Carbon::now()->toDateTimeString();
        $status['order'] = $order->save();

        if (!$status['order']) {
            $result['message'] = "Error creating order!";
            try {
                $order->delete();
            } catch (\Exception $exception) {
                $result['message'] = $exception->getMessage();
            }
            return response()->json($result);
        }

        $total = 0;
        foreach ($request->input('barang') as $value) {
            $orderBarang = new OrderBarang();
            $orderBarang->id_order = $order->id_order;
            $orderBarang->id_barang = $value['id_barang'];
            $orderBarang->quantity = $value['quantity'];
            $status['orderBarang'] = $orderBarang->save();

            if (!$status['orderBarang']) {
                $result['message'] = "Error getting item!";
                try {
                    $order->delete();
                    $orderBarang->delete();
                } catch (\Exception $exception) {
                    $result['message'] = $exception->getMessage();
                }
                return response()->json($result);
            }
            $total = $total + (Barang::where('id_barang', '=', $value['id_barang'])->first()->harga * $value['quantity']);
        }
//        dd($total);

        $detailTransaksi = new DetailTransaksi();
        $detailTransaksi->tanggal_transaksi = Carbon::now()->toDateTimeString();
        $detailTransaksi->status_transaksi = "pending";
        if ($request->input('total_bayar') == $total) {
            $detailTransaksi->total_bayar = $request->input('total_bayar');
            $status['detailTransaksi'] = $detailTransaksi->save();
        } else {
            $status['detailTransaksi'] = false;
        }

        if (!$status['detailTransaksi']) {
            $result['message'] = "Problem fecthing detail information!";
            try {
                $order->delete();
                $orderBarang->delete();
                $detailTransaksi->delete();
            } catch (\Exception $exception) {
                $result['message'] = $exception->getMessage();
            }
            return response()->json($result);
        }

        $transaksi = new Transaksi();
        $transaksi->id_detail_transaksi = $detailTransaksi->id_detail_transaksi;
        $transaksi->id_order = $order->id_order;
        $status['transaksi'] = $transaksi->save();

        if (!$status['transaksi']) {
            $result['message'] = "Error with transaction request! ";
            try {
                $order->delete();
                $orderBarang->delete();
                $detailTransaksi->delete();
                $transaksi->delete();
            } catch (\Exception $exception) {
                $result['message'] = $exception->getMessage();
            }
            return response()->json($result);
        }

        $result['isSuccess'] = true;
        $result['message'] = "Make Transaction Success!";

        return response()->json($result, 200, array(), JSON_PRETTY_PRINT);
    }
}
