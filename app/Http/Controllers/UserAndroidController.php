<?php

namespace App\Http\Controllers;

use App\Models\UserAuthRobotcandy;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserAndroidController extends Controller
{
    public function checkLiveTime(Request $request)
    {
        $robotcandy = UserAuthRobotcandy::all()->where("robotcandy_id_owner", "-", $request->input('id_user'))->first();
//        dd($robotcandy);
//        Carbon::now()->toDateTimeString()
        $time = Carbon::now()->timestamp;
        echo($time . ">=" . $robotcandy->robotcandy_time_live->toDateTimeString());
        if ($time >= $robotcandy->robotcandy_time_live->timestamp) {
            return response()->json("Your Session is Expired, Please login to create new session");
        } else {
            return response()->json("Not Expired");
        }
    }

    public function createDummyTime(Request $request)
    {
//        dd($request);
//        dd(sha1(str_random() . $request->input("id_user") . Carbon::now()->timestamp));
        $now = Carbon::now();
        $robotcandy = new UserAuthRobotcandy();
        $robotcandy->robotcandy_id_owner = $request->input("id_user");
        $robotcandy->robotcandy_auth_token = sha1(str_random() . $request->input("id_user") . $now->timestamp);
        $robotcandy->robotcandy_time_generated = $now->timestamp;
        $robotcandy->robotcandy_time_live = $now->addMonth(1)->timestamp;
        $robotcandy->save();

    }
}
