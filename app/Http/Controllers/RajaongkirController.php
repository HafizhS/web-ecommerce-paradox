<?php

namespace App\Http\Controllers;

use App\Models\RoCity;
use App\Models\RoProvince;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use Illuminate\Http\Request;
use function MongoDB\BSON\fromJSON;

class RajaongkirController extends Controller
{
    public function getProvinceFromRajaongkir()
    {
        if (RoProvince::all()->isNotEmpty()) {
            RoProvince::query()->delete();
        }
        $client = new Client();
        $res = $client->get('https://api.rajaongkir.com/starter/province?key=2f9422428e3c7bad1b089ce9f552ba04');
        $data = \GuzzleHttp\json_decode((string)$res->getBody()->getContents(), true)['rajaongkir']['results'];
        foreach ($data as $val) {
            $roProvince = new RoProvince();
            $roProvince->id_province = $val["province_id"];
            $roProvince->province = $val["province"];
            $roProvince->save();
        }
    }

    public function getCityFromRajaongkir()
    {
        if (RoCity::all()->isNotEmpty()) {
            RoCity::query()->delete();
        }
        $client = new Client();
        $res = $client->get('https://api.rajaongkir.com/starter/city?key=2f9422428e3c7bad1b089ce9f552ba04');
        $data = \GuzzleHttp\json_decode((string)$res->getBody()->getContents(), true)['rajaongkir']['results'];
        foreach ($data as $val) {
            $roCity = new RoCity();
            $roCity->id_city = $val["city_id"];
            $roCity->id_province = $val["province_id"];
            $roCity->province = $val["province"];
            $roCity->type = $val["type"];
            $roCity->city_name = $val["city_name"];
            $roCity->postal_code = $val["postal_code"];
            $roCity->save();
        }
    }

    public function getProvinceRajaongkir()
    {
        $data = RoProvince::all();
        return response()->json($data,200,array(),JSON_PRETTY_PRINT);
    }

    public function getCityRajaongkir()
    {
        $data = RoCity::all();
        return response()->json($data,200,array(),JSON_PRETTY_PRINT);
    }
}