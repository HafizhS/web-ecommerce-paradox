<?php

namespace App\Http\Controllers;

use App\Models\Lookpack;
use App\Models\LookpackItem;
use Illuminate\Http\Request;

class LookpackController extends Controller
{
    public function index(Request $request)
    {
        if (!empty($request->input("jenis_kelamin"))) {
            $lookpack = Lookpack::all()->where("jenis_kelamin", $request->input("jenis_kelamin"));
        } else {
            $lookpack = Lookpack::all();
        }

        foreach ($lookpack as $pack) {
            $image = base64_encode($pack->model_pack);
            $image = str_replace('data:image/jpg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $pack->model_pack = $image;
        }
        return response()->json($lookpack, 200, array(), JSON_PRETTY_PRINT);
    }

    public function lookpackItem(Request $request)
    {
        return LookpackItem::all()->where("id_lookpack", $request->input("id_lookpack"));
    }

    public function lookpackDetail(Request $request)
    {
        $details = \DB::table('barang')
            ->join('lookpack_item', 'lookpack_item.id_barang', '=', 'barang.id_barang')
            ->where('lookpack_item.id_lookpack', '=', $request->input('id_lookpack'))
            ->select('barang.*')
            ->get();
        foreach ($details as $item) {
            $item->gambar_barang = base64_encode($item->gambar_barang);
//            $item->gambar_barang = "gambar";
        }
        return response()->json($details, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getAll()
    {
        $data = \DB::table('lookpack')
            ->select(array('lookpack.*', \DB::raw("(SELECT COUNT(*) FROM `lookpack_item` WHERE `lookpack_item`.`id_lookpack` = `lookpack`.`id_lookpack`) AS 'count'")))
            ->get();
        foreach ($data as $item) {
            $item->model_pack = base64_encode($item->model_pack);
//            $item->gambar_barang = "gambar";
        }
        return view('admin.datalookpack')->with('data', $data);
    }

    public function getWithDetail()
    {
        $data = \DB::table('lookpack')
            ->leftJoin('lookpack_item', 'lookpack_item.id_lookpack', '=', 'lookpack.id_lookpack')
            ->leftJoin('barang', 'barang.id_barang', '=', 'lookpack_item.id_barang')
            ->select('*')
            ->get();
        foreach ($data as $item) {
            $item->gambar_barang = base64_encode($item->gambar_barang);
//            $item->gambar_barang = "gambar";
        }
        return view('admin.datalookpack')->with('data', $data);
    }

    public function store(Request $request)
    {
//        dd($request);
        $lookpack = new Lookpack();
        $lookpack->nama_pack = $request->input('name');
        $lookpack->desc = $request->input('deskripsi');
        $lookpack->model_pack = base64_encode($request->input('image'));
        $lookpack->jenis_kelamin = $request->input('gender');
        $result['lookpack'] = $lookpack->save();

        if ($request->has('items')) {
            foreach ($request->input('items') as $item) {
                $lookpackItem = new LookpackItem();
                $lookpackItem->id_lookpack = $lookpack->id_lookpack;
                $lookpackItem->id_barang = $item;
                $result['lookpackItem'] = $lookpackItem->save();
            }
        }

        if ($result['lookpack'] == 1 || $result['lookpackItem'] == 1) {
            $result['message'] = "Success creating new lookpack ( name: " . $request->input('nama') . ")";
            $result['theme'] = "callout-success";
        } else if ($result['lookpack'] == 0 || $result['lookpackItem'] == 0) {
            $result['message'] = "Failed creating new lookpack (" . $request->input('nama') . ")";
            $result['theme'] = "callout-danger";
        } else {
            $result['message'] = "Unknown result state";
            $result['theme'] = "callout-warning";
        }

        return redirect(route('admin.datalookpack'))->with('result', $result);
    }

    public function create()
    {
        return view('admin.tambahlookpack');
    }
}
