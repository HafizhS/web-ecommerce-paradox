<?php

namespace App\Http\Controllers;

use App\Models\UserAlamat;
use Illuminate\Http\Request;

class AlamatController extends Controller
{
    //

    public function getAlamatByUser(Request $request)
    {
        $alamat = \DB::table("user_alamat")
            ->join("user", 'user.id_user', '=', 'user_alamat.id_user')
            ->where('user_alamat.id_user', '=', $request->input('id_user'))
            ->select('user_alamat.*')
            ->get();
        return response()->json($alamat, 200, array(), JSON_PRETTY_PRINT);
    }

    public function getRoAlamatByIdAlamat(Request $request)
    {
        $roalamat = \DB::table("user_alamat_rajaongkir")
            ->join("user_alamat", "user_alamat.id_user_alamat", '=', "user_alamat_rajaongkir.id_user_alamat")
            ->where("user_alamat_rajaongkir.id_user_alamat", '=', $request->input('id_user_alamat'))
            ->select(["user_alamat_rajaongkir.*","user_alamat.*"])
            ->first();

//        $alamat = UserAlamat::where('id_user_alamat', '=', $request->input('id_user_alamat'))->get();

        return response()->json($roalamat, 200, array(), JSON_PRETTY_PRINT);
    }
}
