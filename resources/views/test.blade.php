<html>
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="{{asset("bower_components/bootstrap/dist/css/bootstrap.min.css")}}">
</head>
<body id="test" class="container-fluid">
<button id="addbutton">ADD</button>
<form action="{{route("testdd")}}" method="post">
    @csrf
    <input name="input[]" type="text">
    <input name="input[]" type="text">
    <input name="input[]" type="text">
    <button type="submit">Submit</button>
</form>
<div>

</div>
</body>
<script type="text/javascript">

    window.onload = function () {
        init();
    };

    function init() {
        var index = 0;
        var input = document.createElement("select");

        var parent = document.getElementById("test");

        input.name = "category[" + index + "]";
        input.id = "category[" + index + "]";

        parent.appendChild(input);
        fillCombobox();

        function createButton() {
            index++;
            var button = document.createElement("select");
            button.id = "category[" + index + "]";
            parent.appendChild(button);
            fillCombobox();
            document.getElementById("category["+index+"]").onclick = function () {
                createButton();
            }
            document.getElementById("addbutton").onclick = function () {
                createButton();
            }

        }

        function fillCombobox() {

                    @foreach($kategoris as $kategori)
            var option = document.createElement("option");
            option.innerHTML = "{{$kategori->nama_kategori}}";
            document.getElementById("category[" + index + "]").appendChild(option);
            @endforeach

        }

        // document.getElementById("category[0]").addEventListener("click", createButton());
    }

</script>
</html>