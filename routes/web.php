<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('login');
});

View::share("theme", 'skin-green');
View::share("theme_layout", 'fixed');

//Barang
Route::post('api/barang', 'BarangController@store')->name("barang.store");
Route::get('api/barang', 'BarangController@index');
Route::get('api/barang/{id}', 'BarangController@update');
Route::post('api/barang/{id}', 'BarangController@destroy');
Route::get('api/test', 'BarangController@getImage');
Route::get('api/test1', 'BarangController@indexWithCategory');

//User
Route::get('api/user', 'UserController@indexApi');
Route::post('api/login', 'UserController@loginApi');
Route::post('api/register', 'UserController@register');
Route::get('api/user/detail', 'UserController@getDetailUserApi');

//Alamat
Route::get('api/user/alamat', 'AlamatController@getAlamatByUser');
Route::get('api/user/alamat/ro', 'AlamatController@getRoAlamatByIdAlamat');

//Lookpack
Route::get('api/lookpack', 'LookpackController@index');
Route::get('api/lookpack/detail', 'LookpackController@lookpackDetail')->name('lookpack.detail');

Route::get('api/test2', function () {
    echo asset("storage/.gitignore");
});

//Keranjang
Route::post('api/keranjang/add', 'KeranjangController@addToKeranjang');
Route::get('api/keranjang', 'KeranjangController@getAllCartItem');
Route::get('api/keranjang/test', 'KeranjangController@getUserCart');

//Kategori
Route::get('api/kategori', 'KategoriController@getAll');

//Transaksi
Route::get('api/transaction', 'TransaksiController@createTransaction');

//Order
Route::get('api/order', 'OrderController@makeOrder');

Route::get('test321', 'UserController@test321');

//Rajaongkir
Route::get('ro/province', 'RajaongkirController@getProvinceFromRajaongkir');
Route::get('ro/city', 'RajaongkirController@getCityFromRajaongkir');
Route::get('api/alamat/province', 'RajaongkirController@getProvinceRajaongkir');
Route::get('api/alamat/city', 'RajaongkirController@getCityRajaongkir');

//Admin
Route::get('admin', function () {
    return view("admin.index");
})->name('admin.index');

Route::post('admin', function () {
    return view("admin.index");
})->name('admin.index');


Route::get('admin/databarang', 'BarangController@indexView')->name("admin.databarang");
Route::get('admin/databarang/tambah', 'BarangController@create')->name('admin.tambahbarang');

Route::get('admin/datauser', 'UserController@index')->name("admin.datauser");
Route::post('admin/datauser/detail', 'UserController@getDetailUser')->name('admin.detailuser');

Route::get('admin/datalookpack', 'LookpackController@getAll')->name('admin.datalookpack');
Route::get('admin/datalookpack/add', 'LookpackController@create')->name('admin.tambahlookpack');
Route::post('admin/datalookpack/add', 'LookpackController@store')->name('admin.tambahlookpack');

Route::get('admin/datakategori', 'KategoriController@indexView')->name('admin.datakategori');
Route::post('admin/datakategori', 'KategoriController@store')->name('admin.tambahkategori');
Route::delete('admin/datakategori', 'KategoriController@delete')->name('admin.deletekategori');
Route::patch('admin/datakategori', 'KategoriController@edit')->name('admin.editkategori');


Route::get('admin/datas', 'BarangController@indexView');
Route::get('lat', 'KategoriController@test123');
Route::post('lat', 'KategoriController@testdd')->name('testdd');

Route::get('censor', 'KeranjangController@test123');

Route::get('test/time', 'UserAndroidController@createDummyTime');
Route::get('test/time/check', 'UserAndroidController@checkLiveTime');

